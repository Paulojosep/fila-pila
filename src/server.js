const express = require('express');
const path = require('path');

const app = express();
app.use(express.static('src'));

app.get('/',(req,res) => {
    res.sendFile(path.join(__dirname,'index.html'));
    res.sendFile('Stack.js');
})

app.listen(3000, () => {
    console.log("Servidor escutando na porta 3000");
})